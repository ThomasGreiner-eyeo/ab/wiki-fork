## Regular release

1. [Create new release](#create-new-release)
1. [Work on release issues](#work-on-release-issues)

1. Feature freeze
   1. [Prepare feature freeze](#prepare-feature-freeze)
   1. **Feature freeze**  
      *No more changes are allowed to land in release branch (excl. fixes).*  
      *No other releases can go into feature freeze until this release has been published.*
   1. [Announce feature freeze](#announce-feature-freeze)
1. [Test release](#test-release)
1. Code freeze
   1. [Prepare code freeze](#prepare-code-freeze)
   1. **Code freeze**  
      *No more changes are allowed to land in release branch.*
1. Release
   1. [Create builds](#create-builds)
   1. [Make release](#make-release)
1. Store reviews
1. [Clean up](#clean-up)

## Create new release

**When:** After entering code freeze of previous release.

- Create milestone:
  - Title: Release name (e.g. `3.12`)
  - Assign milestone to all issues with ~"State::Waiting to be released" label.
- Create [release issue][ui-template-release]:
  - Title: `Release AdBlock <version>` (e.g. `Release AdBlock 3.12`)
  - Assign to milestone.
- Update [release history][wiki-history] accordingly.

## Work on release issues

- [Follow development workflow][wiki-development-workflow] until ~"State::Waiting to be released" label is set.

## Prepare feature freeze

**When:** After determining a target set of issues make up a good release candidate (e.g. based on amount, value).

- Check that there are issues for all agreed on changes.
- Assign milestone to all issues.
- Verify all issues are ready (see also [definition of done][wiki-definition-done]):
  - Ignore unmerged issues with ~"NotReleaseBlocking" label.
  - Has ~"State::Waiting to be released"?
  - References all relevant merge requests under "Related merge requests" or issue which fixes the problem?
  - Has "Hints for testers" section, if necessary?
  - Has "Hints for translators" section, if there were changes to translation files?
- Update repository:
  - Pull master branch.
  - Create release candidate branch
  - Update the target branch MRs of related issues 


## Announce feature freeze
* Request review from relevant stakeholders by adding a comment to the document.
* Update [release information](https://gitlab.com/adblockinc/ext/adblock/releases) accordingly.
* Notify stakeholders in respective channels https://app.slack.com/client/T01R10CDEJY/C01TMC4PM0E
  * Broadcast message: `we have entered feature freeze for AdBlock {version}: {link}`
  * Link to GitLab milestone for stakeholders

## Test release

* Assign ~"State::Feature QA" label to release issue.
* Sync up with QA once per week.
* Wait for QA LGTM.

## Prepare release

**When:** After release issue gets assigned ~"State::Waiting to be released" label.

* Check back with QA to get final QA LGTM.
* Verify all release-related issues are ready to be released
* Close GitLab issues and remove ~"State::Waiting to be released" label (except release issue).
* Close milestone.
* Check whether any third-party issues are confidential and ask to make them public.
* Check whether any issues are confidential and verify that they can be made public at the time of the release.
* Update GitLab and local repository:
  * Using the MR, merge the release-candidate branch onto main branch. Uncheck the 'Squash commits when merge request is accepted.'
  * Locally, switch to a shipping only repository
  * Locally, in the adblock repository:
     * pull `main` branch
     * delete the 'node_modules' folder
     * run `npm run submodules-update`
     * run 'npm install'
     * clean up any artifacts in the dependency folders ( in each folder under 'vendor', perform a `git reset --hard`)
  * In the adjacent shipping repository, run the script `ship-chrome prepit`
    * The script will update version string:
       * Check that you are on the main branch without any outstanding changes
       * Prompt you for the next version
       * Create a new branch called release-branch-X-Y
       * Modify the "version" in /build/config/base.mjs file.
       * Modify the "version" in /adblock-betafish/CHANGELOG.txt file.
       * Allow you to add information to the CHANGELOG
       * get the latest translators.json from the i18n repository
       * make a tag for the release 
       * Create a build files (zip and xpi files) for the extension stores
       * Verify that automatically generated JavaScript bundles in builds are smaller than 4MB.
  * Run shipping script `ship-chrome reallshipit`
    * The script will:
      * Verifying that you ran `ship-chrome prepit` before it
      * Push the branch and tag to the gitlab repo
      * Create a development build for each browser, and FTP the files to the code.getadblock.com/releases/ directory
  * In GitLab, create an MR from the `release-branch-X-Y` branch
    **NOTE** The MR should not used squash commits.  Uncheck the 'Squash commits when merge request is accepted.'
  * Merge the new branch into the main branch
  * Verify latest CI pipeline for main branch succeeded
  * Up date the local repository with the changes in the main branch

  * Run the following command to create a source build (needed for the AMO):
    * npx gulp source
  * Notify team on Slack of successful release build, request updating of open merge requests.

** Submit release
  * Publish to both beta and stable channel on the CWS(https://chrome.google.com/webstore/developer/dashboard).
     * Note: two different zip files should have been created by the ship-chrome script
  * If necessary, update meta data before publishing.
  * Check missing information.
  * Upload build file and publish (see [details](#upload-cws)).
  * Verify that review has started.
* Publish to [AMO](https://addons.mozilla.org/en-US/developers/addons).
  * Upload build and source code file and publish (see [details](#upload-amo)).
  * Check validation warnings/errors.
  * Verify that review has started.
* Publish to [Microsoft store](https://partner.microsoft.com/en-us/dashboard/microsoftedge/overview).
  * Upload build file and publish (see [details](#upload-microsoft-store)).
  * Verify that review has started.

### Upload: AMO

1. Click "Upload new version" button.
1. Upload **XPI file**.
1. Click "Continue" button.
1. Select "Yes" to indicate that source code needs to be submitted.
1. Upload **TAR.GZ file** from the source step above
1. Click "Continue" button.
1. Ignore input fields.
1. Click "Submit version" button.

### Upload: CWS

1. In "Package" tab:
   1. Upload **ZIP file**.
   1. Verify that there have been no changes to permissions compared to prior version.
1. In "Store listing" tab: Click "Submit for review" button.

### Upload: Microsoft store

1. Click on "Update" button.
1. Click on "Replace" and upload **ZIP file**.
1. Verify that only the following permissions are listed:  
   `<all_urls>, contextMenus, notifications, storage, tabs, unlimitedStorage, webNavigation, webRequest, webRequestBlocking`
1. Click on "Continue" button.
1. Click on "Publish" button.
1. Ignore input fields and click on "Publish" button.

## Clean up

**When:** After release is submitted on all stores.

* Remove note about reviews from release notes.
* Close all related issues and remove ~"State::Waiting to be released" label.
* Notify stakeholders in Slack(https://app.slack.com/client/T01R10CDEJY/C01TMC4PM0E).
  * Broadcast message: `AdBlock {version} has been submitted on all stores`